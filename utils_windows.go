package gnuplot

import (
  "path/filepath"
  "strings"
)

func filepathForGnuplot(fpath string) string {
  fpath = filepath.FromSlash(fpath)
  return strings.Replace(fpath, "\\", "\\\\", -1)
}
